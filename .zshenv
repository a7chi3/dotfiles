#!/usr/bin/env sh

ZDOTDIR=$XDG_CONFIG_HOME/zsh/
[[ -f "$ZDOTDIR/.zshenv" ]] || source "$ZDOTDIR/.zshenv"
